// In JS, we define a class by using keyword "class" followed by "class name", and a set of {}

// Naming convention dictates that class names should begin with an uppercase letter
class Student {
    // Constructor - method defines how objects will be instantiated from a class
    constructor(name, email, grades) {
        this.name = name;
        this.email = email;

        /*
            ACTIVITY 1
            Define a grades property in our Student class that will accept an array of 4 numbers ranging from 0 to 100 to be its value. If the argument used does not satisfy all the conditions given, this property will be set to undefined instead.
        */
        if (grades.length === 4) {
            if (grades.every(grade => grade >= 0 && grade <= 100)) {
                this.grades = grades;
            }
        } else {
            this.grades = undefined
        }

        this.gradeAve = undefined;
        this.pass = undefined;
        this.passWithHonor = undefined;
    }

    // Methods are defined outside the constructor method and they are not separated by commas as with objects
    login() {
        console.log(`${this.email} has logged in`);
        return this;
    }

    logout() {
        console.log(`${this.email} has logged out`);
        return this;
    }

    listGrades() {
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
        return this;
    }

    computeAve() {
        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        // return sum/4;
        this.gradeAve = sum / 4;
        return this;
    }

    willPass() {
        this.pass = this.computeAve() >= 85 ? true : false;
        return this;
    }

    willPassWithHonors() {
        this.passWithHonor = (this.willPass() && this.computeAve() >= 90) ? true : false;
        return this;
    }



}

// To create objects from a class, we use the keyword "new"
// Object instantiation
// let studentOne = new Student("John", "john@mail.com");
// let studentTwo = new Student("Joe", "joe@mail.com");

/*
    ACTIVITY 2
    Instantiate all four students from the previous session from our Student class. Use the same values as before for their names, emails, and grades.
*/
    let studentOne = new Student("John", "john@mail.com", [89, 84, 78, 88]);
    let studentTwo = new Student("Joe", "joe@mail.com", [78, 82, 79, 85]);
    let studentThree = new Student("Jane", "jane@mail.com", [87, 89, 91, 93]);
    let studentFour = new Student("Jessie", "jessie@mail.com", [91, 89, 92, 93]);

/*
    What is the blueprint where objects are created from? - classes
    What is the naming convention applied to classes? - Uppercase letter
    What keyword do we use to create objects from a class? - new
    What is the technical term for creating an object from a class? - object instantiation
    What class method dictates HOW objects will be created from that class? - constructor
*/

// [SECTION] Function Coding
// 1. Define a grades property in our Student class that will accept an array of 4 numbers ranging from 0 to 100 to be its value. If the argument used does not satisfy all the conditions given, this property will be set to undefined instead.
// 2. Instantiate all four students from the previous session from our Student class. Use the same values as before for their names, emails, and grades.