// [SECTION] QUIZ
//1. How do you create arrays in JS?
	//let arrayName = ["array1", "array2", ... , "arrayN"];
	let array = ["abc", "def", "ghi", "jkl"];

//2. How do you access the first character of an array?
console.log(array[0]);

//3. How do you access the last character of an array?
console.log(array[array.length - 1]);

//4. What array method searches for, and returns the index of a given value in an array? This method returns -1 if given value is not found in the array.
console.log(array.indexOf("def"));

//5. What array method loops over all elements of an array, performing a user-defined function on each iteration?
array.forEach(function(element) {
  console.log(element);
});

//6. What array method creates a new array with elements obtained from a user-defined function?
let newArray = array.map(function(element){
	return element.toUpperCase();
});

console.log(newArray);

//7. What array method checks if all its elements satisfy a given condition?
let allLower = array.every(function(element) {
	return element === element.toLowerCase();
})

console.log(allLower);

//8. What array method checks if at least one of its elements satisfies a given condition?
let hasLower = array.some(function(element) {
	return element !== element.toLowerCase();
})

console.log(hasLower);

//9. True or False: array.splice() modifies a copy of the array, leaving the original unchanged.
// False

//10. True or False: array.slice() copies elements from original array and returns them as a new array.
//True

// [SECTION] Function Coding
// 1. Create a function named addToEnd that will add a passed in string to the end of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Ryan" as arguments when testing.
let students = ["John", "Joe", "Jane", "Jessie"];

let addToEnd = (array, element) => {
	if (typeof element !== "string") {
		return "error - can only add strings to an array";
	};

	array.push(element);

	console.log(array);
};

// addToEnd(students, "Ryan");
// addToEnd(students, 045);

// 2. Create a function named addToStart that will add a passed in string to the start of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Tess" as arguments when testing.
let addToStart = (array, element) => {
	if (typeof element !== "string") {
		return "error - can only add strings to an array";
	};

	array.unshift(element);

	console.log(array);
};

// addToStart(students, "Tess");
// addToStart(students, 033);

// 3. Create a function named elementChecker that will check a passed in array if at least one of its elements has the same value as a passed in argument. If array is empty, return the message "error - passed in array is empty". Otherwise, return a boolean value depending on the result of the check. Use the students array and the string "Jane" as arguments when testing.
let elementChecker = (array, element) => {
	if (typeof element !== "string") {
		return "error - can only add strings to an array";
	};

	return array.includes(element);
}

/* 
	4. Create a function named checkAllStringsEnding that will accept a passed in array and a character. The function will do the ff:
		- if array is empty, return "error - array must NOT be empty"
		- if at least one array element is NOT a string, return "error - all array elements must be strings"
		- if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
		- if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
		- if every element in the array ends in the passed in character, return true. Otherwise return false.
	Use the students array and the character "e" as arguments when testing.Tip - string characters may be accessed by passing in an index just like in arrays.
*/
let checkAllStringsEnding = (array, element) => {
	if (array.length === 0) {
		return "error - array must NOT be empty";
	}

	if (!array.every(arrayElement => typeof arrayElement === "string")) {
		return "error - all array elements must be strings"
	}

	if (typeof element !== "string") {
		return "error - 2nd argument must be of data type string"
	}

	if (element.length !== 1) {
		return "error - 2nd argument must be a single character"
	}

	return array.every(arrayElement => arrayElement[arrayElement.length - 1] === element);
}

// 5. Create a function named stringLengthSorter that will take in an array of strings as its argument and sort its elements in an ascending order based on their lengths. If at least one element is not a string, return "error - all array elements must be strings". Otherwise, return the sorted array. Use the students array to test.
let stringLengthSorter = (array) => {
	if (!array.every(element => typeof element === "string")) {
		return "error - all array elements must be strings"
	}

	return array.sort((a, b) => a.length - b.length);
}

/*
	6. Create a function named startsWithCounter that will take in an array of strings and a single character. The function will do the ff:
		- if array is empty, return "error - array must NOT be empty"
		- if at least one array element is NOT a string, return "error - all array elements must be strings"
		- if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
		- if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
		- return the number of elements in the array that start with the character argument, must be case-insensitive
	 Use the students array and the character "J" as arguments when testing.
*/
let startsWithCounter = (array, element) => {
	if (array.length === 0) {
		return "error - array must NOT be empty";
	}

	if (!array.every(arrayElement => typeof arrayElement === "string")) {
		return "error - all array elements must be strings"
	}

	if (typeof element !== "string") {
		return "error - 2nd argument must be of data type string"
	}

	if (element.length !== 1) {
		return "error - 2nd argument must be a single character"
	}

	let lowercaseElement = element.toLowerCase();
	return array.filter(arrayElement => arrayElement.charAt(0).toLowerCase() === lowercaseElement).length;
}

/*
	7. Create a function named likeFinder that will take in an array of strings and a string to be searched for. The function will do the ff:
		- if array is empty, return "error - array must NOT be empty"
		- if at least one array element is NOT a string, return "error - all array elements must be strings"
		- if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
		- return a new array containing all elements of the array argument that contain the string argument in it, must be case-insensitive
	 Use the students array and the string "jo" as arguments when testing.
*/
let likeFinder = (array, element) => {
	if (array.length === 0) {
		return "error - array must NOT be empty";
	}

	if (!array.every(arrayElement => typeof arrayElement === "string")) {
    	return "error - all array elements must be strings";
  	}

  	if (typeof element !== "string") {
    	return "error - 2nd argument must be of data type string";
  	}

  	let lowercaseElement = element.toLowerCase();
  	return array.filter(arrayElement => arrayElement.toLowerCase().includes(lowercaseElement));
};

// 8. Create a function named randomPicker that will take in an array and output any one of its elements at random when invoked. Pass in the students array as an argument when testing.
let randomPicker = (array) => {
  	if (array.length === 0) {
    	return "error - array must NOT be empty";
  	}

  	let random = Math.floor(Math.random() * array.length);
  	return array[random];
};
