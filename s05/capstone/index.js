// [SECTION] Instructions
/*
    Simulate the following basic e-commerce functionalities through the use of classes and objects:
        customer registration - customer class
        add product - product class
        update product price - product class
        archive product - product class
        add to cart - cart class
        show cart - cart class
        update cart quantity - cart class
        clear cart - cart class
        compute cart total - cart class
        check out - cart class
*/
/*
    1. Customer class:
        email property - string
        cart property - instance of Cart class
        orders property - array of objects with stucture {products: cart contents, totalAmount: cart total}
        checkOut() method - pushes the Cart instance to the orders array if Cart contents is not empty
*/
/*
    2. Product class
        name property - string
        price property - number
        isActive property - Boolean: defaults to true
        archive() method - will set isActive to false if it is true to begin with
        updatePrice() method - replaces product price with passed in numerical value
*/
/*
    3. Cart class:
        contents property - array of objects with structure: {product: instance of Product class, quantity: number}
        totalAmount property - number
        addToCart() method - accepts a Product instance and a quantity number as arguments, pushes an object with structure: {product: instance of Product class, quantity: number} to the contents property
        showCartContents() method - logs the contents property in the console
        updateProductQuantity() method - takes in a string used to find a product in the cart by name, and a new quantity. Replaces the quantity of the found product in the cart with the new value.
        clearCartContents() method - empties the cart contents
        computeTotal() method - iterates over every product in the cart, multiplying product price with their respective quantities. Sums up all results and sets value as totalAmount.
*/

// [SECTION] Function Code
// Customer Class
class Customer {
    constructor(email) {
        this.email = email;
        this.cart = new Cart();
        this.orders = [];
    }

    checkOut() {
        if (this.cart.contents.length > 0) {
            const order = {
                products: this.cart.contents,
                totalAmount: this.cart.totalAmount
            }; // mirrors the contents of the cart
            this.orders.push(order); //pushes to order array
            this.cart.clearCartContents(); //clears the cart contents
        }
        return this;
    }
}

// Product Class
class Product {
    constructor(name, price) {
        this.name = name;
        this.price = price;
        this.isActive = true;
    }

    archive() {
        this.isActive = false; // sets the isActive property to false
        return this;
    }

    updatePrice(price) {
        this.price = price; // updates the price to the desired new price
        return this;
    }
}

// Card Class
class Cart {
    constructor() {
        this.contents = [];
        this.totalAmount = 0;
    }

    addToCart(product, quantity) {
        this.contents.push({ product, quantity }); //pushes to contents array
        this.computeTotal(); //uses the computeTotal method
        return this;
    }

    showCartContents() {
        console.log(this.contents); //logs the contents
    }
    
    updateProductQuantity(productName, newQuantity) {
        const productIndex = this.contents.findIndex(item => item.product.name === productName); //finds the index within the array of the product
        if (productIndex !== -1) { //tells that the located product is within the array
            this.contents[productIndex].quantity = newQuantity; //sets the new quantity
            this.computeTotal(); //recomputes the new total
        }
        return this;
    }

    clearCartContents() {
        this.contents = []; //sets the array to empty
        this.totalAmount = 0; // sets the new total to 0
        return this;
    }

    computeTotal() {
        this.totalAmount = 0; //resets the totalAmount
        this.contents.forEach(content => {
            this.totalAmount += content.product.price * content.quantity; //computes for the totalAmount
        })
        return this;
    }
}

// [SECTION] Instantiation
const john = new Customer('john@mail.com');
const prodA = new Product('soap', 9.99);
john.cart.addToCart(prodA, 6);