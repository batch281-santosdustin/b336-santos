// [SECTION] QUIZ
// 1. What is the term given to unorganized code that's very hard to work with?
    // Spaghetti Code

// 2. How are object literals written in JS?
    // In curly braces
    let object = {
        key1: "value1",
        key2: "value2"
    };

// 3. What do you call the concept of organizing information and functionality to belong to an object?
    // OOP - Object-oriented programming

// 4. If the studentOne object has a method named enroll(), how would you invoke it?
    // studentOne.enroll();

// 5. True or False: Objects can have objects as properties.
    // True

// 6. What is the syntax in creating key-value pairs?
    // key: value

// 7. True or False: A method can have no parameters and still work.
    // True

// 8. True or False: Arrays can have objects as elements.
    // True

// 9. True or False: Arrays are objects.
    // True

// 10. True or False: Objects can have arrays as properties.
    // True

// [SECTION] Code

// BOILERPLATE
    //create student one
    let studentOneName = 'John';
    let studentOneEmail = 'john@mail.com';
    let studentOneGrades = [89, 84, 78, 88];

    //create student two
    let studentTwoName = 'Joe';
    let studentTwoEmail = 'joe@mail.com';
    let studentTwoGrades = [78, 82, 79, 85];

    //create student three
    let studentThreeName = 'Jane';
    let studentThreeEmail = 'jane@mail.com';
    let studentThreeGrades = [87, 89, 91, 93];

    //create student four
    let studentFourName = 'Jessie';
    let studentFourEmail = 'jessie@mail.com';
    let studentFourGrades = [91, 89, 92, 93];

    //actions that students may perform will be lumped together
    function login(email){
        console.log(`${email} has logged in`);
    }

    function logout(email){
        console.log(`${email} has logged out`);
    }

    function listGrades(grades){
        grades.forEach(grade => {
            console.log(grade);
        })
    }

// 1. Translate the other students from our boilerplate code into their own respective objects.
// 2. Define a method for EACH student object that will compute for their grade average (total of grades divided by 4)
// 3. Define a method for all student objects named willPass() that returns a Boolean value indicating if student will pass or fail. For a student to pass, their ave. grade must be greater than or equal to 85.
// 4. Define a method for all student objects named willPassWithHonors() that returns true if ave. grade is greater than or equal to 90, false if >= 85 but < 90, and undefined if < 85 (since student will not pass).

let createStudent = (name, email, grades) => {
    return {
        name,
        email,
        grades,

        computeAve() {
            let total = this.grades.reduce((sum, grade) => sum + grade, 0);
            return total / this.grades.length;

        },

        willPass() {
            return this.computeAve() >= 85;
        },

        willPassWithHonors() {
            if (this.computeAve() >= 90) {
                return true;
            } else if (this.computeAve() >= 85) {
                return false;
            } else {
                return undefined;
            }
        }
    }
}

let studentOne = createStudent("John", "john@mail.com", [89, 84, 78, 88]);
let studentTwo = createStudent("Joe", "joe@mail.com", [78, 82, 79, 85]);
let studentThree = createStudent("Jane", "jane@mail.com", [87, 89, 91, 93]);
let studentFour = createStudent("Jessie", "jessie@mail.com", [91, 89, 92, 93]);

// 5. Create an object named classOf1A with a property named students which is an array containing all 4 student objects in it.
// 6. Create a method for the object classOf1A named countHonorStudents() that will return the number of honor students.
// 7. Create a method for the object classOf1A named honorsPercentage() that will return the % of honor students from the batch's total number of students.
// 8. Create a method for the object classOf1A named retrieveHonorStudentInfo() that will return all honor students' emails and ave. grades as an array of objects.
    let classOf1A = {
        students: [studentOne, studentTwo, studentThree, studentFour],

        countHonorStudents() {
            let honorCount = 0;
            for (let student of this.students) {
                if(student.willPassWithHonors()) {
                    honorCount++;
                }
            }

            return honorCount;
        },

        honorsPercentage() {
            return this.countHonorStudents() / this.students.length * 100;
        },

        retrieveHonorStudentInfo() {
            let honorStudent = [];
            for (let student of this.students) {
                if (student.willPassWithHonors()) {
                    honorStudent.push({
                        email: student.email,
                        average: student.computeAve()
                    });
                }
            }

            return honorStudent;
        },

        sortHonorStudentByGradeDesc() {
            let honorStudent = this.retrieveHonorStudentInfo();
            honorStudent.sort((a, b) => b.average - a.average);
            return honorStudent;
        }
    };





// 9. Create a method for the object classOf1A named sortHonorStudentsByGradeDesc() that will return all honor students' emails and ave. grades as an array of objects sorted in descending order based on their grade averages.